# End-to-end test of a workadventure instance setup via NixOS modules.

{ fediventure, pkgs, lib, ... }:

let
  test = import "${pkgs.path}/nixos/tests/make-test-python.nix" ({ pkgs, libs, ... }: {
    name = "workadventure-e2e";
    nodes = {
      machine = { ... }: {
        imports = [
          ../nixos/modules/base.nix
          ../nixos/modules/workadventure/workadventure.nix
        ];
        networking.interfaces.eth0.useDHCP = true;
        services.openssh.permitRootLogin = "yes";
        users.users.root.password = "root";

        fediventure.tree = fediventure;
        services.workadventure.instances.test = {
          nginx.default = true;
          nginx.domain = "localhost";
        };
      };
    };

    testScript = ''
      start_all()
      machine.wait_for_unit("wa-back-test.service")
      machine.wait_for_open_port(8080)
    '';
  });
in test { inherit pkgs; inherit (pkgs) libs; }
