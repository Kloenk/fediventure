{ config, pkgs, ... }:

{
  imports = [
    ./modules/vm-kurdebele.nix
    ./modules/matrix-test.nix
  ];
  networking.hostName = "matrixtest2";
  networking.interfaces.enp1s0 = {
    ipv6.addresses = [
      { address = "2a01:4f9:4a:4319:1337::13"; prefixLength = 80; }
    ];
    ipv4.addresses = [
      { address = "135.181.235.221"; prefixLength = 29; }
    ];
  };
  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/eae3ea89-8752-4576-b954-ef110974e23e";
      fsType = "ext4";
    };
    "/boot" = {
      device = "/dev/disk/by-uuid/DD7B-ECBB";
      fsType = "vfat";
    };
  };
  swapDevices = [
    { device = "/dev/disk/by-uuid/f371daaa-4ce6-4ced-a315-f90c8b88f922"; }
  ];
  system.stateVersion = "20.09";
}
