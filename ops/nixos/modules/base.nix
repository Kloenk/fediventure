{ config, pkgs, lib, ... }:

{
  options = {
    # Fediventure-specific options. These are populated by
    # ops.deploy.configurations when building configurations into top-level
    # derivations.
    fediventure = {
      # readTree of repository root. NixOS module can access
      # `config.fediventure.tree.foo` to get to `foo` in the repository.
      tree = lib.mkOption { description = "fediventure top-level repository tree"; };
    };
  };
  config = {
    networking.domain = "i.fediventure.net";
    time.timeZone = "Europe/Amsterdam";
    networking.useDHCP = false;
    users.users.root = {
      openssh.authorizedKeys.keys = with config.fediventure.tree.ops.admins; sshkeys groups.rootOnProd;
    };
    environment.systemPackages = with pkgs; [
      wget vim rxvt_unicode.terminfo git htop
    ];
    programs.mtr.enable = true;
    services.openssh.enable = true;

    security.acme.email = "q3k@hackerpace.pl";
    security.acme.acceptTerms = true;
  };
}
