# Generic configuration for VMs running on kurdebele.q3k.org, a Hetzner machine
# in HEL1-DC4.
# The host is managed by q3k.

{ config, pkgs, modulesPath, ... }:

{
  imports = [
    ./base.nix
    (modulesPath + "/profiles/qemu-guest.nix")
  ];
  boot.initrd.availableKernelModules = [ "ahci" "xhci_pci" "virtio_pci" "sr_mod" "virtio_blk" ];
  boot.initrd.kernelModules = [];
  boot.kernelModules = [];
  boot.extraModulePackages = [];

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  networking.defaultGateway = "135.181.235.217";
  networking.defaultGateway6 = "2a01:4f9:4a:4319:1337::1";
  networking.nameservers = [
    "213.133.99.99"
    "213.133.100.100"
    "213.133.98.98"
    "2a01:4f8:0:1::add:9898"
    "2a01:4f8:0:1::add:1010"
    "2a01:4f8:0:1::add:9999"
  ];
}
