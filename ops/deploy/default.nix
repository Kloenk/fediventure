{ pkgs, lib, fediventure, ... }:

with builtins;

let
  machines = [
    ../nixos/prod1.nix
    ../nixos/matrixtest1.nix
    ../nixos/matrixtest2.nix
  ];

  configurations = listToAttrs (map (m: rec {
    name = value.config.networking.hostName;
    value = pkgs.nixos ({ config, options, ... }: {
      imports = [ m ];
      config = {
        fediventure.tree = fediventure;
      };
    });
  }) machines);

in rec {
  inherit configurations;
}
