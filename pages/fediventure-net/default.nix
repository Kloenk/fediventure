{ pkgs, fediventure, ... }:

let
  pages = fediventure.pages;

in with pkgs; {
  server = pages.mkServer "fediventure.net-devserver" ./.;
  build = pages.mkBuild "fediventure.net-build" ./.;
}
