{ pkgs, stdenv, ... }:

rec {
  inherit (pkgs.python38Packages) lektor;

  # mkServer makes a script that will run a lektor development server against
  # a given lektor project. The path given will be leaked into an absolute path
  # on the filesystem (via builtins.toString) to ensure that the dev server is
  # run against a live version from the filesystem, and not a copy within the
  # nix store.
  mkServer = name: path: pkgs.writeScriptBin name ''
    ${lektor}/bin/lektor --project=${builtins.toString path} server -v
  '';

  # mkBuild returns a derivation that will build a given lektor project into
  # static files to be hosted via HTTP.
  mkBuild = name: path: stdenv.mkDerivation {
    inherit name;

    buildCommand = ''
      mkdir -p $out
      ${lektor}/bin/lektor \
        --project=${path} \
        build \
        --output-path=$out
    '';
  };
}
